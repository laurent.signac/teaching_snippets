data =  [ (1, "Minerva"), (2, "Albus"), (3, "Severus"), (4, "Sibylle"), (5, "Gilderoy") ]

def nb_voyelles(nom: str) -> int:
    nom_m = nom.lower()
    c = 0
    for voyelle in "aeiou":
        c = c + nom_m.count(voyelle)
    return c

# Affiche le nombre de voyelles contenu dans chaque non
for nom in data:
    print(nb_voyelles(nom))
