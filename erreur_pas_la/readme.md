# L'erreur n'est pas là

Le code ne fonctionne pas. Mais l'endroit où l'erreur est signalée est simplement l'endroit où la faut est révélée. 
La véritable erreur est ailleurs. Trouvez la faute et expliquez le comportement observé.
