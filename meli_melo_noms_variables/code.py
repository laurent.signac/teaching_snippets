from PIL import Image

image = Image.open("sample_image.png")

for a in range(image.size[0]):
    for b in range(image.size[1]):
        r, v, b = image.getpixel((a, b))
        # Traitement pour assombrir par exemple : 
        r1, v1, b1 = r//2, v//2, b//2
        image.putpixel((a, b), (r1, v1, b1))

image.show()
