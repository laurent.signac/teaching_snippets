# Méli mélo de noms de variables

Exécuté sur l'image `sample_image.png` aucune exception n'est levée. 
Mais le résultat obtenu n'est pas celui escompté (l'image contient des tââches).

Exécuté sur l'image `small_sample_image.png`, une exception est levée.

Pouvez-vous expliquer le problème (c'est pus important que le résoudre).
